import { Query } from "react-apollo";
import { CURRENT_USER_QUERY } from "./User";
import SignIn from "./Signin";

const PleaseSignIn = props => (
  <Query query={CURRENT_USER_QUERY}>
    {({ data, error, loading }) => {
      if (loading) return <div>Loading...</div>;

      if (error) return <div>Error!</div>;

      if (!data.me) {
        return (
          <div>
            <p>Please SignIn before continuing</p>
            <SignIn />
          </div>
        );
      }

      return props.children;
    }}
  </Query>
);

export default PleaseSignIn;
