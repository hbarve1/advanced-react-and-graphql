import React, { Component } from "react";
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import { adopt } from "react-adopt";

import User from "./User";
import CartItem from "./CartItem";

import CartStyle from "./styles/CartStyles";
import Supreme from "./styles/Supreme";
import CloseButton from "./styles/CloseButton";
import SickButton from "./styles/SickButton";

import calTotalPrice from "../lib/calcTotalPrice";
import formatMoney from "../lib/formatMoney";
import TakeMyMoney from "./TakeMyMoney";

export const LOCAL_STATE_QUERY = gql`
  query {
    cartOpen @client
  }
`;

export const TOGGLE_CART_MUTATION = gql`
  mutation {
    toggleCart @client
  }
`;

const Compose = adopt({
  user: ({ render }) => <User>{render}</User>,
  toggleCart: ({ render }) => (
    <Mutation mutation={TOGGLE_CART_MUTATION}>{render}</Mutation>
  ),
  localState: ({ render }) => <Query query={LOCAL_STATE_QUERY}>{render}</Query>,
});

class Cart extends Component {
  render() {
    return (
      <Compose>
        {({ user, toggleCart, localState }) => {
          const me = user.data.me;
          if (!me) {
            return null;
          }

          return (
            <CartStyle open={localState.data.cartOpen}>
              <header>
                <CloseButton title="close" onClick={toggleCart}>
                  &times;
                </CloseButton>
                <Supreme>{me.name}'s Cart</Supreme>
                <p>
                  You have {me.cart.length} Item
                  {me.cart.length ? "" : "s"} in your cart.
                </p>
              </header>
              <ul>
                {me.cart.map(cartItem => (
                  <CartItem cartItem={cartItem} key={cartItem.id} />
                ))}
              </ul>
              <footer>
                <p>{formatMoney(calTotalPrice(me.cart))}</p>
                {me.cart.length ? (
                  <TakeMyMoney>
                    <SickButton>Checkout</SickButton>
                  </TakeMyMoney>
                ): null}
              </footer>
            </CartStyle>
          );
        }}
      </Compose>
    );
  }
}

export default Cart;
