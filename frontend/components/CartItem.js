import React, { Component } from "react";
import styled from "styled-components";

import RemoveFromCart from "./RemoveFromCart";
import formatMoney from "../lib/formatMoney";

const CartItemStyle = styled.li`
  padding: 1rem 0;
  border-bottom: 1px solid ${props => props.theme.lightgrey};
  display: grid;
  align-items: center;
  grid-template-columns: auto 1fr auto;

  img {
    margin-right: 10px;
  }
  h3,
  p {
    margin: 0px;
  }
`;

class CartItem extends Component {
  render() {
    const { cartItem } = this.props;

    if (!cartItem.item) {
      return (
        <CartItemStyle>
          <p>This item has been removed.</p>
          <RemoveFromCart id={cartItem.id} />
        </CartItemStyle>
      );
    }

    return (
      <CartItemStyle>
        <img width={100} src={cartItem.item.image} alt={cartItem.item.title} />
        <div className="cart-item-details">
          <h3>{cartItem.item.title}</h3>
          <p>
            {formatMoney(cartItem.item.price * cartItem.quantity)}
            {" - "}
            <em>
              {cartItem.quantity} &times; {formatMoney(cartItem.item.price)}{" "}
              each
            </em>
          </p>
        </div>
        <RemoveFromCart id={cartItem.id} />
      </CartItemStyle>
    );
  }
}

export default CartItem;
