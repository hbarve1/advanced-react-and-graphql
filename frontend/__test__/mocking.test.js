function Person(name, foods) {
  this.name = name;
  this.foods = foods;
}

Person.prototype.fetchFavFood = function() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(this.foods);
    }, 2000);
  });
};

describe("mocking learnign", () => {
  it("mocks a reg function", () => {
    const fetchDogs = jest.fn();

    fetchDogs("snickers");
    expect(fetchDogs).toHaveBeenCalled();
    expect(fetchDogs).toHaveBeenCalledWith("snickers");

    fetchDogs("hugo");
    expect(fetchDogs).toHaveBeenCalledTimes(2);
  });

  it("can create a person", () => {
    const me = new Person("Himank", ["burger", "pizza"]);

    expect(me.name).toBe("Himank");
  });

  it("can fetch foods", async () => {
    const me = new Person("Himank", ["burger", "pizz"]);

    me.fetchFavFood = jest.fn().mockResolvedValue(["sushi", "ramen"]);

    const favFoods = await me.fetchFavFood();

    // console.log(favFoods);
    expect(favFoods).toContain("sushi");
  });
});
