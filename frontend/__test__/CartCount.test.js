import { shallow, mount } from "enzyme";
import toJSON from "enzyme-to-json";
import CartCount from "../components/CartCount";

describe("<CartCount />", () => {
  it("renders", () => {
    shallow(<CartCount count={100} />);
  });

  it("matches the snapshot", () => {
    const wrapper = shallow(<CartCount count={10} />);

    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it("update via props", () => {
    const wrapper = shallow(<CartCount count={50} />);

    expect(toJSON(wrapper)).toMatchSnapshot();
    wrapper.setProps({ count: 60 });
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
});
