import { shallow, mount } from "enzyme";
import toJSON from "enzyme-to-json";

import ItemComponent from "../components/Item";

const fakeItem = {
  id: "abc123",
  title: "Cool Item",
  description: "Cool Item Description",
  price: 5000,
  image: "dog.jpg",
  largeImage: "large-dog.jpg",
};

describe("<Item />", () => {
  it("renders and matches the snapshot", () => {
    const wrapper = shallow(<ItemComponent item={fakeItem} />);

    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  // it("renders the image properly", () => {
  //   const wrapper = shallow(<ItemComponent item={fakeItem} />);
  //   const img = wrapper.find("img");

  //   expect(img.props().src).toBe(fakeItem.image);
  // });

  // it("it renders the price tags and title", () => {
  //   const wrapper = shallow(<ItemComponent item={fakeItem} />);

  //   const PriceTag = wrapper.find("PriceTag");

  //   expect(PriceTag.children().text()).toBe("$50");

  //   expect(wrapper.find("Title a").text()).toBe(fakeItem.title);
  // });

  // it("renders out the button properly", () => {
  //   const wrapper = shallow(<ItemComponent item={fakeItem} />);

  //   const buttonList = wrapper.find(".buttonList");

  //   expect(buttonList.children()).toHaveLength(3);
  //   expect(buttonList.find("Link")).toHaveLength(1);
  //   expect(buttonList.find("AddToCart").exists()).toBe(true);
  //   expect(buttonList.find("DeleteItem").exists()).toBe(true);
  // });
});
