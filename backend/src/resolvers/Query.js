const { forwardTo } = require("prisma-binding");
const { hasPermission } = require("../utils");

const Query = {
  // async items(parent, args, ctx, info) {
  //   const items = await ctx.db.query.items();

  //   return items;
  // },
  items: forwardTo("db"),
  itemsConnection: forwardTo("db"),
  item: forwardTo("db"),
  async me(parent, args, ctx, info) {
    // check if there is a current user ID
    if (!ctx.request.userId) {
      return null;
    }

    const res = await ctx.db.query.user(
      {
        where: {
          id: ctx.request.userId,
        },
      },
      info
    );

    return res;
  },
  async users(parent, args, ctx, info) {
    // 1. checked if they are logged in
    if (!ctx.request.userId) {
      throw new Error("You must be logged in!");
    }

    // 2. check if the user has the permission to query all the users
    hasPermission(ctx.request.user, ["ADMIN", "PERMISSIONUPDATE"]);

    // 3. if they do, query all the users!
    return ctx.db.query.users({}, info);
  },
  async order(parent, args, ctx, info) {
    // 1. make sure they are logged in
    if (!ctx.request.userId) {
      throw new Error("You are not logged in.");
    }

    // 2. query the current order
    const order = await ctx.db.query.order(
      {
        where: {
          id: args.id,
        },
      },
      info
    );

    // 3. check if the have the permissions to see this order
    const ownsOrder = order.user.id === ctx.request.userId;
    const hasPermissionToSeeOrder = ctx.request.user.permissions.includes(
      "ADMIN"
    );

    if (!ownsOrder && !hasPermissionToSeeOrder) {
      throw new Error("You can't see this buddy");
    }

    // 4. return the order
    return order;
  },
  async orders(parents, args, ctx, info) {
    const { userId } = ctx.request;
    if (!userId) {
      throw new Error("You must be signed In.");
    }

    return ctx.db.query.orders(
      {
        where: {
          user: {
            id: userId,
          },
        },
      },
      info
    );
  },
};

module.exports = Query;
