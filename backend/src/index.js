const cookieParser = require("cookie-parser");
const jwt = require("jsonwebtoken");

require("dotenv").config({ path: "variables.env" });

const createServer = require("./createServer");

const db = require("./db");

const server = createServer();

server.express.use(cookieParser());

// decode the JWT so we can get the user Id on each request
server.express.use((req, res, next) => {
  const { token } = req.cookies;
  if (token) {
    const { userId } = jwt.verify(token, process.env.APP_SECRET);
    // put the userId onto the req for future requests to access
    req.userId = userId;
  }
  next();
});

// create a middleware which populates the user to each request
server.express.use(async (req, res, next) => {
  const { userId } = req;

  // if they are not signed in, skip this.
  if (!userId) {
    return next();
  }

  const user = await db.query.user(
    {
      where: {
        id: userId,
      },
    },
    "{ id name email permissions }"
  );

  req.user = user;
  next();
});

//start it
server.start(
  {
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL,
    },
  },
  ({ port }) => {
    console.log(`Server is running on port http://localhost:${port}`);
  }
);
